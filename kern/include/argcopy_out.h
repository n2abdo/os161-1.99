#ifndef ARGCOPY_OUT
#define ARGCOPY_OUT

#include <types.h>
#include <kern/errno.h>
#include <kern/fcntl.h>
#include <lib.h>
#include <proc.h>
#include <current.h>
#include <addrspace.h>
#include <vm.h>
#include <vfs.h>
#include <syscall.h>
#include <test.h>
#include "opt-A3.h"
#include <copyinout.h>

userptr_t argcopy_out(int argc, char ** strAdd, vaddr_t * stackptr);
#endif // CHECK_H
